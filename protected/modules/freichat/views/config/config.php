<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$settings = Yii::$app->getModule('freichat')->settings;
$token = $settings->get('token');
$parts = explode("$$", $token);

function getApiLink($parts) {
    $clientId = $parts[0];
    $secret = $parts[1];
    $currTime = time();

    $identity = Yii::$app->user->identity;
    $name = $identity->getDisplayName();
    $email = $identity->email;
    $data = json_encode([
        "siteName" => Yii::$app->name,
        "userDisplayName" => $name,
        "email" => $email,
        "time" => $currTime
    ]);

    $key_size = 32; // 256 bits for AES-256
    $salt = random_bytes($key_size);
    $key = hash_pbkdf2("sha1", $secret, $salt, 100000, $key_size, true);

    $method = "AES-256-CBC";
    $ivLen = openssl_cipher_iv_length($method);
    $iv = random_bytes($ivLen);
    $encryptedData = openssl_encrypt($data, $method, $key, OPENSSL_RAW_DATA, $iv);
    $encodedData = $clientId . "$$" . urlencode(base64_encode($iv . $salt . $encryptedData));

    return "https://app.freichat.com/management/authorized/register?data=$encodedData";
}

$api = false;
if (count ($parts) > 1) {
    $api = getApiLink($parts);
}

?>

<div class="panel panel-default">

    <div class="panel-heading"><?= Yii::t('FreichatModule.base', 'FreiChat configuration'); ?></div>

    <div class="panel-body">

        <?php $form = ActiveForm::begin(['id' => 'configure-form']); ?>
        <div class="form-group">
            <?= $form->field($model, 'token')->textInput(['class' => 'form-control', 'disabled' => false])->label(false) ?>

        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('FreichatModule.base', 'Save'), ['class' => 'btn btn-primary', 'data-ui-loader' => '']); ?>
            <a class="btn btn-default"
               href="<?= Url::to(['/admin/module']); ?>"><?= \Yii::t('FreichatModule.base', 'Back to modules'); ?></a>
        </div>
        <?php if ($api)  { ?>
            <br/><br/>
            <hr>
            <div class="form-group">
                <label><?= \Yii::t('FreichatModule.base', 'Please visit our app backend for configuring FreiChat:'); ?></label><br/>
                <a class="btn btn-success" target="_blank" id="openBackendBtn"
                   href="<?= Url::to($api); ?>"><?= \Yii::t('FreichatModule.base', 'Open App Backend'); ?></a>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<script type="text/javascript" <?= \humhub\libs\Html::nonce() ?>>
    function disableBackendButtonAfter10Minutes() {
        const openBackendBtn = document.getElementById("openBackendBtn");
        openBackendBtn.className = "btn btn-default";
        openBackendBtn.addEventListener("click", function(e) {
            e.preventDefault();
            alert("For security reasons we have disabled this button. Please refresh the page to activate it again.");
            return false;
        });
    }
    window.setTimeout(disableBackendButtonAfter10Minutes, 1000 * 60 * 10);
</script>
