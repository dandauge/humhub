Changelog
=========

1.0.3 - Nov 3, 2020
------------------------
 - Add friends integration
 - Add japanese translations
 - Add/Fix avatar integration
 - Add menu button to open chat (commented)
 - Add link to freichat app backend


1.0.0 - Aug 7, 2019
------------------------
 - Add integration to load current user
 - Generate saas token and register with saas server automatically
