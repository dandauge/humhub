<?php
return array (
  'Active' => 'Aktív',
  'Mark as unseen for all users' => 'Jelölje meg "Nem olvasottnak" minden felhasználónak',
  'Message' => 'Üzenet',
  'Title' => 'Tárgy',
);
