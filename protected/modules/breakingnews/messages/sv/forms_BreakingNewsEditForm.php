<?php
return array (
  'Active' => 'Aktiv',
  'Mark as unseen for all users' => 'Markera som osedd för alla användare',
  'Message' => 'Meddelande',
  'Title' => 'Rubrik',
);
